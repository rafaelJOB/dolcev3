import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.StringTokenizer;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;

public class Synset {
	
	protected Concept concept;
	private IDictionary dict;
	
	private List<String> stopWords;
	private List<String> adjWords;
	
	protected void set_concept(Concept _concept) {
		concept = _concept;
	}
	
	protected void set_dictionary(IDictionary _dict) {
		dict = _dict;
	}
	
	protected void set_stpWords(List<String> _stopWords) {
		stopWords = _stopWords;
	}
	
	protected List<String> get_stpWords() {
		return stopWords;
	}
	
	protected void set_adjWords(List<String> _adjWords) {
		adjWords = _adjWords;
	}
	
	protected List<String> get_adjWords() {
		return adjWords;
	}
	
	/*recupera o synset do conceito*/
	void recover_goodSynset() throws IOException {
		
		List<String> context = concept.transfer_context();
		String name = concept.get_className().toString();
		dict.open();
		IIndexWord idxWord = dict.getIndexWord(name,POS.NOUN);
		
		HashMap<ISynset, List<String>> temp1 = new HashMap<ISynset, List<String>>();
		
		if(idxWord != null) {
			int numSy = 0;
			int max = 0;
			List<IWordID> wordIds = idxWord.getWordIDs();
			
			for (IWordID wordId : wordIds) {
				IWord word = dict.getWord (wordId) ;
			    ISynset synset = word.getSynset();
			    		
			    /*Cria uma lista com as sids do synset */
			    List<IWord> wordsSynset = synset.getWords();
			    String glossSynset = synset.getGloss();
			    		
			    /*Retorna uma lista com o BagOfWords do synset*/
			    List<String> bagSynset = createBagWords(wordsSynset,glossSynset);
			    
			    temp1.put(synset, bagSynset);

			    /*Metodo que compara o Contexto com o BagOfWords do synset*/
			    int size = intersection(context, bagSynset);
			    if(size > max) {
			    	max = size;
			    	concept.set_goodSynset(synset);
						//System.out.println(max);
			    }
			    numSy++;
			}
			//System.out.println("Concept: " + concept.get_className());
		    //concept.print_goodSynset();
		    //System.out.println("----\n");
			concept.set_numSy(numSy);
		}
		concept.set_synsetCntx(temp1);
		dict.close();
	}
	
	public void recover_goodSynsetComp_1() throws IOException {
		
		int max = 0;
		List<String> wordCList = new ArrayList<String>();		
		List<String> context = concept.transfer_context();
		List<String> List = new ArrayList<String>();
		
		//System.out.println("Concept: " + concept.get_className() + "\n");
		
		wordCList = separate(concept.get_className().toString());
		List = remove_stopWords(wordCList);		// remove as stopwords
		wordCList.clear();

		wordCList = remove_adj(List);				//remove os adj da lista
		
		//concept.show_context();
		//System.out.println("\n");
		
		dict.open();
		
		for(String wordC: wordCList) {

			IIndexWord idxWord = dict.getIndexWord(wordC,POS.NOUN);

			HashMap<ISynset, List<String>> temp1 = new HashMap<ISynset, List<String>>();
			if(idxWord != null) {
				int numSy = 0;
				List<IWordID> wordIds = idxWord.getWordIDs();
				
				for (IWordID wordId : wordIds) {

					IWord word = dict.getWord (wordId) ;
				    ISynset synset = word.getSynset(); 

				    /*Cria uma lista com as sids do synset */
				    List<IWord> wordsSynset = synset.getWords();
				    String glossSynset = synset.getGloss();
				    		
				    /*Retorna uma lista com o BagOfWords do synset*/
				    List<String> bagSynset = createBagWords(wordsSynset,glossSynset);
				    
				    temp1.put(synset, bagSynset);

				    /*Metodo que compara o Contexto com o BagOfWords do synset*/
				    int size = intersection(context, bagSynset);
				    if(size > max) {
				    	max = size;
				    	concept.set_goodSynset(synset);
				    }
				    numSy++;

				}
				concept.set_numSy(numSy);
			}
			concept.set_synsetCntx(temp1);
		}
	    
		dict.close();
	}
	
	public void recover_goodSynsetComp_2() throws IOException {
		
		int max = 0;
		String wordC;		
		List<String> context = concept.transfer_context();
		
		wordC= separate_2(concept.get_className().toString());
		
		dict.open();
		
		IIndexWord idxWord = dict.getIndexWord(wordC,POS.NOUN);

		LinkedHashMap<ISynset, List<String>> temp1 = new LinkedHashMap<ISynset, List<String>>();
		if(idxWord != null) {
			int numSy = 0;
			List<IWordID> wordIds = idxWord.getWordIDs();
				
			for (IWordID wordId : wordIds) {

				IWord word = dict.getWord (wordId) ;
			    ISynset synset = word.getSynset(); 

			    /*Cria uma lista com as sids do synset */
			    List<IWord> wordsSynset = synset.getWords();
			    String glossSynset = synset.getGloss();
				    		
			    /*Retorna uma lista com o BagOfWords do synset*/
			    List<String> bagSynset = createBagWords(wordsSynset,glossSynset);
				    
			    temp1.put(synset, bagSynset);

			    /*Metodo que compara o Contexto com o BagOfWords do synset*/
			    int size = intersection(context, bagSynset);
			    if(size > max) {
			    	max = size;
			    	concept.set_goodSynset(synset);
			    }
			    numSy++;

			}
			concept.set_numSy(numSy);
		}
		concept.set_synsetCntx(temp1);
		dict.close();
	}
	
	public void recover_goodSynsetComp_3() throws IOException {
		
		int max = 0;
		String wordC;		
		List<String> context = concept.transfer_context();
		
		wordC = separate_3(concept.get_className().toString());
		
		dict.open();
		
		IIndexWord idxWord = dict.getIndexWord(wordC,POS.NOUN);

		HashMap<ISynset, List<String>> temp1 = new HashMap<ISynset, List<String>>();
		if(idxWord != null) {
			int numSy = 0;
			List<IWordID> wordIds = idxWord.getWordIDs();
				
			for (IWordID wordId : wordIds) {

				IWord word = dict.getWord (wordId) ;
			    ISynset synset = word.getSynset(); 

			    /*Cria uma lista com as sids do synset */
			    List<IWord> wordsSynset = synset.getWords();
			    String glossSynset = synset.getGloss();
				    		
			    /*Retorna uma lista com o BagOfWords do synset*/
			    List<String> bagSynset = createBagWords(wordsSynset,glossSynset);
				    
			    temp1.put(synset, bagSynset);

			    /*Metodo que compara o Contexto com o BagOfWords do synset*/
			    int size = intersection(context, bagSynset);
			    if(size > max) {
			    	max = size;
			    	concept.set_goodSynset(synset);
			    }
			    numSy++;

			}
			concept.set_numSy(numSy);
		}
		concept.set_synsetCntx(temp1);
		dict.close();
	}
	
	private List<String> createBagWords(List<IWord> wordsSynset, String glossSynset) {
	    List<String> list = new ArrayList<String>();
	    
	    for (IWord i : wordsSynset) {
	    	StringTokenizer st = new StringTokenizer(i.getLemma().toLowerCase().replace("_"," ")," ");
	    	while (st.hasMoreTokens()) {
	    		  String token = st.nextToken();
	    	 	  if (!list.contains(token)) {
	    	  	      list.add(token);
	    	      }
	    	}
	    }
	    glossSynset = glossSynset.replaceAll(";"," ").replaceAll("\"", " ").replaceAll("-"," ").toLowerCase();
	    //System.out.println(stopWords);
	    StringTokenizer st = new StringTokenizer(glossSynset," ");
    	while (st.hasMoreTokens()) {
    		   String token = st.nextToken().toLowerCase();
    		   token = remove_specialchar(token);
    		   if (!stopWords.contains(token) && !list.contains(token)) {			
    			   list.add(token);
    		   }
    	}
	    
	   return list;
	}
	
	int intersection(List<String> context, List<String> bagSynset) {
		int inter = 0;
		for(String word: context) {
			//System.out.println(word);
			word = word.toLowerCase();
			for(String wordCompared: bagSynset) {
				if(word.equals(wordCompared)) {
					inter++;
				}
			}
		}
		return inter;
	}

	List<String> remove_adj(List<String> wordList) {
		
		List<String> temp = new ArrayList<String>();  
		    for(String word: wordList) {
		    	temp.add(word);
				if(adjWords.contains(word.toLowerCase())) {
		    		temp.remove(word);
		    	}
		    }    
		    wordList.clear();
		    wordList = temp;
		    return wordList;
	}
	
	/*remove as stopwords de uma list*/
	List<String> remove_stopWords(List<String> wordCList) {
		    
		List<String> temp = new ArrayList<String>();
			for(String wordC: wordCList) {

		        if(!stopWords.contains(wordC.toLowerCase())) {
		        	temp.add(wordC.toLowerCase());
		        	//System.out.println("SEM STOP: " + wordC);     
		        }
		        		          
		    }
			wordCList.clear();
			wordCList = temp;
			return wordCList;
	}
	
	/*remove caracteres especiais*/
	String remove_specialchar(String word) {
		
		if(word.contains("(")) {
        	word = word.replace("(", "");
        }
        
        if(word.contains(")")) {
        	word = word.replace(")", "");
        }
        
        if(word.contains(",")) {
        	word = word.replace(",", "");
        }
        
        if(word.contains(":")) {
        	word = word.replace(":", "");
        }        
        
        if(word.contains("'")) {
        	word = word.replace("'", "");
        }
        
        if(word.contains(".")) {
        	word = word.replace(".", "");
        }
        
        /*if(word.contains("?")) {
        	word = word.replace("?","");
        }*/
        
        return word;	
	}
	
	private List<String> separate(String wordComp) {
		
		List<String> wordS = new ArrayList<String>();
		String words[];
		int i;
		
		wordComp = wordComp.replace("_", " ");
		words = wordComp.split(" ");
		i = words.length;
		
		/*for(String word: words) {
			System.out.println(word + " - Separate");
		}*/
		
		wordS.add(words[i - 1].toLowerCase());
		return wordS;
	}
	
	private String separate_2(String wordComp) {
		
		String word = null;
		int x = wordComp.length();
		int up = 0;
		for(int y = 1; y < x; y++) {
			if(Character.isUpperCase(wordComp.charAt(y)) && y > up) {
				up = y;	
			}	
		}
		if(up != 0) {
			word = wordComp.substring(up);
		}
		
		return word;
	}
	
	private String separate_3(String wordComp) {
		
		String words[];
		int i;
		
		wordComp = wordComp.replace("-", " ");
		words = wordComp.split(" ");
		i = words.length;
		
		return words[i - 1].toLowerCase();
	}

}

