import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;

import edu.mit.jwi.item.ISynset;

public class Concept {
	
	
	private OWLClass owlClass;
	private String ontologyID;
	private String classID;
	
	private String ontologyName;
	private String className;
	
	private Map<String, Integer> distance;				//Relaciona o nome da classe com sua distacia da classe "principal"
	private String desc;
	private Set<String> context;					//Cria um SET(n�o permite itens repitidos) para o contexto utiliza o HASH para melhor performance de adi��o de itens
	
	private List<String>stpWords;
	
	private List<OWLClassExpression> supers;
	private List<OWLClassExpression> subs;
	
	private ISynset goodSynset;
	
	private Map<ISynset, List<String>> synsetCntxt;
	
	private OWLClass aliClass;
	
	private int numSy;
	
	public Concept() {
		distance = new TreeMap<String, Integer>();
		context = new HashSet<String>();
		synsetCntxt = new HashMap<ISynset, List<String>>();
		stpWords = new ArrayList<String>();
		numSy = -1;
	}
	
	void set_owlClass(OWLClass owlclass) {
		owlClass = owlclass; 
	}
	
	OWLClass get_owlClass() {
		return owlClass;
	}
	
	void set_ontologyID(String _ontologyID) {
		ontologyID = _ontologyID;
	}
	
	String get_ontologyID() {
		return ontologyID;
	}

	void set_classID(String _classID) {
		classID = _classID;
	}
	
	String get_classID() {
		return classID;
	}
	
	void set_ontologyName(String _ontologyName) {
		ontologyName = _ontologyName;
	}
	
	String get_ontologyName() {
		return ontologyName;
	}
	
	void set_className(String _className) {
		className = _className;
	}
	
	String get_className() {
		return className;
	}
	
	void set_distance(TreeMap<String, Integer> map) {
		distance = map;
	}
	
	Map<String, Integer> get_distance() {
		return distance;
	}
	
	void set_desc(String _desc) {
		desc = _desc; 
	}
	
	String get_desc() {
		return desc;
	}
	
	void set_context(HashSet<String> set) {
		context = set;
	}
	
	Set<String> get_context() {
		return context;
	}
	
	void set_stpWords(List<String> list) {
		stpWords = list;
	}
	
	List<String> get_stpWords() {
		return stpWords;
	}
	
	void set_supers(List<OWLClassExpression> _supers) {
		supers = _supers;
	}
	
	List<OWLClassExpression> get_supers() {
		return supers;
	}
	
	void set_subs(List<OWLClassExpression> _subs) {
		subs = _subs;
	}
	
	List<OWLClassExpression> get_subs() {
		return subs;
	}
	
	void set_goodSynset(ISynset _goodSynset) {
		goodSynset = _goodSynset;
	}
	
	ISynset get_goodSynset() {
		return goodSynset;
	}
	
	void set_synsetCntx(HashMap<ISynset, List<String>> _synsetCntxt) {
		synsetCntxt = _synsetCntxt;
	}
	
	Map<ISynset, List<String>> get_synsetCntx() {
		return synsetCntxt;
	}
	
	void set_aliClass(OWLClass _aliclass) {
		aliClass = _aliclass; 
	}
	
	OWLClass get_aliClass() {
		return aliClass;
	}
	
	void set_numSy(int num) {
		numSy = num;
	}
	
	int get_numSy() {
		return numSy;
	}
	
	void print_goodSynset() {
		System.out.println("Synset: " + goodSynset.toString());
		System.out.println("Gloss: " + goodSynset.getGloss().toString());
		System.out.println("Desc: " + desc);
		String out = "Contexto:";
		for(String a: context) {
			out = out + " " + a;
		}
		System.out.println(out);
	}
	
	protected void init() {
		remove_stopWords();
		
		Set<String> temp = new HashSet<String>();
		for(String c: context) {
			
			//System.out.println("Pr� processado: " + c);
			List<String> sep = new ArrayList<String>();
			
			if(c.contains("_")) {
				separate(sep, c);

				for(String word: sep) {
					temp.add(word.toLowerCase());
				}
				
				sep.clear();
			} else if(hasUp(c)) {
				separate_2(sep, c);
				
				for(String word: sep) {
					temp.add(word.toLowerCase());
				}
				sep.clear();
			} else {
				temp.add(c.toLowerCase());
			}
		}
		context.clear();
		context = temp;
		remove_stopWords();		//remove possiveis stopwordss
	}
	
	/*pr�-processamento, cria��o do context e remo��o das stopwords */
	private void remove_stopWords() {
		
		    HashSet<String> wordSet = new HashSet<String>();   
			for(String temp: context) {
				
			    //System.out.println("anota��o pre:" + temp);
		        String[] words = temp.split(" ");
		        //List<String> wordsList = new ArrayList<String>();

		        for(String word : words) {
		            //String wordCompare = word.toLowerCase();

		        	word = remove_specialchar(word);
		                    
		            String wordCompare = word.toLowerCase();
		            //System.out.println(wordCompare);

		            if(!stpWords.contains(wordCompare)) {
		                //wordsList.add(word);
		                
		                if(word.equals(" ") || word.equals("-") || word.equals("")) {
		                	//System.out.println( "Tem igual");
		                } else {
		                	wordSet.add(word);
		                }
		                //wordSet.add(word);
		            }
		        }
		        
		    }
			context.clear();
	        context = wordSet;
	}
	
	private void separate(List<String> sep, String wordComp) {
		
		String words[];
		
		wordComp = wordComp.replaceAll("_", " ");
		//System.out.println(wordComp);
		words = wordComp.split(" ");
		for(String word: words) {
			sep.add(word);
		}
	}
	
	private void separate_2(List<String> sep, String wordComp) {
		
		int x = wordComp.length();
		int up, aux = 0;
		
		for(int y = 1; y < x; y++) {
			if(Character.isUpperCase(wordComp.charAt(y))) {
				up = y;
				sep.add(wordComp.substring(aux, up).toLowerCase());
				aux = up;
			}	
		}
		sep.add(wordComp.substring(aux).toLowerCase());
	}
	
	String remove_specialchar(String word) {
		if(word.endsWith("-")) {
			word = word.replace("-", "");
		}
		
		if(word.contains("(")) {
        	word = word.replace("(", "");
        }
        
        if(word.contains(")")) {
        	word = word.replace(")", "");
        }
        
        if(word.contains(",")) {
        	word = word.replace(",", "");
        }
        
        if(word.contains(":")) {
        	word = word.replace(":", "");
        }        
        
        if(word.contains("'")) {
        	word = word.replace("'", "");
        }
        
        return word;	
	}
	
	List<String> transfer_context() {
		List<String> list = new ArrayList<String>();
		
		for(String ctxt: context) {
			list.add(ctxt);
		}
		return list;
	}
	
	public boolean hasUp(String word) {
		
		int x = word.length();
		
		for(int y = 1; y < x; y++) {
			if(Character.isUpperCase(word.charAt(y))) {
				return true;
			}	
		}
		return false;	
	}
	
}
